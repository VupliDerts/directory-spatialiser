import bpy
import os
import packcircles as pc
import math
import glob
import bmesh
import random
from math import pi
from datetime import datetime
from statistics import mean 

# get file location and parent directory
file_loc = os.path.dirname(os.path.realpath(__file__))
#dir_loc = os.path.abspath(os.path.join(file_loc, os.pardir))
# you can use any directory on your computer
dir_loc = r"C:\Program Files\Blender Foundation\Blender 3.5"

tag_col=[1.0,0.0,0.0,1.0]
height_scale = 100
text_max = 10
text_min = 1
dir_scale_exp = 0.1
scale_fac = 1
spacing = 0.9
text_scale_factor = 0.25
tags_present = False
key_scale = 1
key_offset = 1.4
statistics = True
scene_setup = True
col_dir = False
col_saturation = 0.25
now = datetime.now()
date_str = now.strftime("%Y.%m.%d")
time_str = now.strftime("%H:%M:%S")
scene = bpy.context.scene

tags = []
dirs = []
sizes = []
bytes = []
names = []
depths = []
tagged = []
locs = []

for ob in bpy.data.objects:
    bpy.data.objects.remove(ob, do_unlink=True)

def setup_scene():
    
        for obj in sc_coll.objects:
            bpy.data.objects.remove(obj)
            
        sun_data = bpy.data.lights.new('sun', 'SUN')
        sun_data.energy = 4.0
        sun_obj = bpy.data.objects.new('Sun', sun_data)
        sc_coll.objects.link(sun_obj)
        sun_obj.rotation_euler=(pi/4,pi*2*60/360,0)
        
        cam_target = bpy.data.objects.new('target',None)
        cam_target.location = (0,0,0)
        sc_coll.objects.link(cam_target)
        
        cam_data = bpy.data.cameras.new('camera')
        cam_data.type = 'ORTHO'
        cam_obj = bpy.data.objects.new('Camera',cam_data)
        sc_coll.objects.link(cam_obj)
        cam_obj.location = (0,min(locs)[0]*2,max(locs)[0]*2)

        cam_data.ortho_scale = (max(locs)[0] - min(locs)[0]) * 2
        
        cam_obj.constraints.new('TRACK_TO')
        cam_obj.constraints["Track To"].target = cam_target
        
        bpy.ops.mesh.primitive_plane_add(size=max(locs)[0]*100, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
        ground = bpy.context.selected_objects[0]
        ground.name = 'Ground'
        #bpy.context.scene.collection.objects.unlink(ground)
        sc_coll.objects.link(ground)
        ground.location = (0,0,0)
        
        # set up ground material and nodes
        gr_mat = bpy.data.materials.new('Ground')
        ground.data.materials.append(gr_mat)
        gr_mat.use_nodes = True
        gr_mat.node_tree.nodes["Principled BSDF"].inputs[0].default_value = [0.075000, 0.075000, 0.075000, 1.000000] 
        
# check for cylinder base mesh

if bpy.data.meshes.get('cylinder') is not None:
    data = bpy.data.meshes['cylinder']
else:
    bpy.ops.mesh.primitive_cylinder_add(radius=1, depth=2, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
    bpy.ops.object.shade_smooth(use_auto_smooth=True)
    data = bpy.context.object.data
    data.name = 'cylinder'
    data.use_fake_user = True

# check for collections
if bpy.data.collections.get('Directories') is not None:
    dir_coll = bpy.data.collections['Directories']
else:
    dir_coll = bpy.data.collections.new('Directories')
    bpy.context.scene.collection.children.link(dir_coll)
        
if bpy.data.collections.get('Labels') is not None:
    lab_coll = bpy.data.collections['Labels']
else:
    lab_coll = bpy.data.collections.new('Labels')
    bpy.context.scene.collection.children.link(lab_coll)
        
if bpy.data.collections.get('Key') is not None:
    key_coll = bpy.data.collections['Key']
else:
    key_coll = bpy.data.collections.new('Key')
    bpy.context.scene.collection.children.link(key_coll)

# check for text label material
if bpy.data.materials.get('Text') is not None:
    text_mat = bpy.data.materials['Text']
else:
    text_mat = bpy.data.materials.new('Text')
    text_mat.diffuse_color = [0.0, 0.0, 0.0, 1.0]
    text_mat.use_fake_user = True

# check for directory material    
if bpy.data.materials.get('Dir') is not None:
    dir_mat = bpy.data.materials['Dir']
else:
    # set up material and nodes
    dir_mat = bpy.data.materials.new('Dir')
    dir_mat.diffuse_color = [0.25, 0.25, 0.25, 1.0]
    dir_mat.use_fake_user = True
    dir_mat.use_nodes = True
    
    attr = dir_mat.node_tree.nodes.new('ShaderNodeAttribute')
    attr.attribute_name = 'tag'
    attr.attribute_type = 'OBJECT'
    attr.location = (-700,0)
    
    attr2 = dir_mat.node_tree.nodes.new('ShaderNodeAttribute')
    attr2.name = 'folder'
    attr2.attribute_name = 'dir_float'
    attr2.attribute_type = 'OBJECT'
    attr2.location = (-700,350)
    
    colramp = dir_mat.node_tree.nodes.new('ShaderNodeValToRGB')
    colramp.color_ramp.interpolation = 'EASE'
    colramp.color_ramp.elements.new(0.5)
    colramp.color_ramp.elements[1].color = [1.0, 1.0, 1.0, 1.0]
    colramp.color_ramp.elements[2].color = [0.5, 0.5, 0.5, 1.0]
    colramp.location = (-500,250)
    
    rgb = dir_mat.node_tree.nodes.new('ShaderNodeRGB')
    rgb.outputs[0].default_value = [1.0, 0.0, 0.0, 1.0]
    rgb.location = (-450,-120)
    
    mix = dir_mat.node_tree.nodes.new('ShaderNodeMix')
    mix.data_type = 'RGBA'
    mix.clamp_factor = False
    mix.inputs[6].default_value = [1.0, 1.0, 1.0, 1.0]
    mix.location = (-200,50)
    
    bsdf = dir_mat.node_tree.nodes['Principled BSDF']
    
    hsv = dir_mat.node_tree.nodes.new('ShaderNodeHueSaturation')
    hsv.inputs[4].default_value = [1-col_saturation, 1.000000, 1.000000, 1.000000]
    hsv.location = (-200,350)
    
    #connect sockets
    dir_mat.node_tree.links.new(attr.outputs[2]    ,colramp.inputs[0])
    dir_mat.node_tree.links.new(attr2.outputs[2]   ,hsv.inputs[0])
    dir_mat.node_tree.links.new(colramp.outputs[0] ,mix.inputs[0])
    dir_mat.node_tree.links.new(mix.outputs[2]     ,bsdf.inputs[0])
    dir_mat.node_tree.links.new(rgb.outputs[0]     ,mix.inputs[7])
    
    if col_dir == True:
        dir_mat.node_tree.links.new(hsv.outputs[0]     ,bsdf.inputs[0])
        

# get lengths of existing directories for user console printout progress
len_dirs = len(dir_coll.objects)
len_labs = len(lab_coll.objects)
len_keys = len(key_coll.objects)

# clear out previous data from collections
i=0
for obj in dir_coll.objects:
    print('purging:',obj.name,i,"out of", len_dirs)
    bpy.data.objects.remove(obj)
    i+=1
i=0
for obj in lab_coll.objects:
    print('purging:',obj.name,i,"out of", len_labs)
    bpy.data.objects.remove(obj)
    i+=1
i=0
for obj in key_coll.objects:
    print('purging:',obj.name,i,"out of", len_keys)
    bpy.data.objects.remove(obj)
    i+=1
i=0

# clear orphan data
bpy.ops.outliner.orphans_purge(do_local_ids=True, do_linked_ids=True, do_recursive=False)

# function to get the size of a directory (excluding sub directories)
def get_dir_size(path='.'):
    total = 0
    with os.scandir(path) as it:
        for entry in it:
            if entry.is_file():
                total += entry.stat().st_size
            # subdirectories
            #elif entry.is_dir():
                #total += get_dir_size(entry.path)
    return total

# function to format bytes to MB, GB, TB etc.
def format_bytes(size):
    # 2**10 = 1024
    power = 2**10
    n = 0
    power_labels = {0 : '', 1: 'K', 2: 'M', 3: 'G', 4: 'T'}
    while size > power:
        size /= power
        n += 1
    return str(round(size)) + power_labels[n] + 'B'

# function to find a text file in a directory
def readme(root):
    for file in os.listdir(root):
        name = file.lower()
        if name.startswith('readme'):
            for t in tags:
                if(search_str(file,t))==True:
                    return(True)

# function to find a word in a text file
def search_str(file_path, word):
    with open(os.path.join(root,file_path), 'r') as file:
        # read all content of a file
        content = file.read()
        # check if string present in a file
        if word in content:
            global tags_present
            tags_present = True
            return True
        else:
            return False

# go recursively through directory to see the extents of directory structure
print("scanning directories, if this is a large folder system it may take some time...")

for root, dir_names, file_names in os.walk(dir_loc):
    print('processing:',os.path.basename(os.path.normpath(root)))
    depth = depth = os.path.normpath(root).count(os.sep)
    byte = get_dir_size(root)
    size = (get_dir_size(root) ** dir_scale_exp) * scale_fac
    if size == 0:
        size = 1 * scale_fac
    name = str(abs(hash(root)))
    if readme(root) == True:
        tagged.append(1.0)
    else:
        tagged.append(0.0)
    bytes.append(byte)
    dirs.append(root)
    depths.append(depth)
    names.append(name)
    sizes.append(size)

circ_gen = pc.pack(sizes)
circles = list(circ_gen)

i=0
for ob in sizes:
    # cylinder object
    obj = bpy.data.objects.new('dir',data)
    obj.name = names[i]
    print('processing:',obj.name)
    obj.location[0] = circles[i][0]
    obj.location[1] = circles[i][1]
    obj.scale[0] = sizes[i] * spacing
    obj.scale[1] = sizes[i] * spacing
    obj.scale[2] = height_scale/depths[i]
    
    parent = str(abs(hash(os.path.abspath(os.path.join(dirs[i], os.pardir)))))
    random.seed(abs(hash(os.path.abspath(os.path.join(dirs[i], os.pardir)))))
    rand_float = random.random()
    
    obj['path'] = dirs[i]
    obj['parent'] = parent
    obj['tag'] = tagged[i]
    obj['col'] = tag_col
    obj['dir_float'] = rand_float
    
    if i > 0:
        if bpy.data.objects[parent]['tag'] > 0:
            obj['tag'] = bpy.data.objects[parent]['tag'] / 2
    if obj.data.materials.get('Dir') is None:
        obj.data.materials.append(dir_mat)
    dir_coll.objects.link(obj)
    
    # text label
    font_curve = bpy.data.curves.new(type="FONT", name="Font Curve")
    font_curve.body = os.path.basename(os.path.normpath(dirs[i])) + '\n' + str(format_bytes(bytes[i]))
    font_curve.align_x = 'CENTER'
    font_curve.align_y = 'CENTER'
    font_obj = bpy.data.objects.new(name="Font Object", object_data=font_curve)
    font_obj.location[0] = circles[i][0]
    font_obj.location[1] = circles[i][1]
    font_obj.location[2] = height_scale/depths[i] + 0.01
    text_scale = sizes[i] * text_scale_factor
    
    # scale text max(min(my_value, max_value), min_value)
    text_scale = max(min(text_scale, text_max), text_min)
    font_obj.scale = (text_scale, text_scale, 1)
    font_obj.data.materials.append(text_mat)
    font_obj.name = names[i]+'_label'
    lab_coll.objects.link(font_obj)    
    i+=1

# get legend location
for ob in dir_coll.objects:
    x = ob.location[0]
    y = ob.location[1]        
    locs.append((x,y))               
leg_x = (min(locs, key = lambda i : i[0])[0]) * key_offset
leg_y = (min(locs, key = lambda i : i[1])[1]) * key_offset


# add a legend if there are tags
if tags_present == True:
    print('tags present')
    
    # addd key object
    bpy.ops.mesh.primitive_cube_add(size=1, enter_editmode=False, align='WORLD', location=(leg_x, -leg_y, 0), scale=(3, 3, 1))
    bpy.ops.object.shade_smooth(use_auto_smooth=True)
    key = bpy.context.object
    key.name = 'Key'
    key.data.materials.append(dir_mat)
    key['tag'] = 0.5
    key['col'] = tag_col
    key.scale[0] = max(sizes) * key_scale
    key.scale[1] = max(sizes) * key_scale
    curr_coll = bpy.context.view_layer.active_layer_collection.collection
    curr_coll.objects.unlink(key)
    key_coll.objects.link(key)
    
    # add key label
    font_curve = bpy.data.curves.new(type="FONT", name="Font Curve")
    font_curve.body = os.path.basename('key:'+'\n'+str(tags))
    font_curve.align_x = 'CENTER'
    font_curve.align_y = 'CENTER'
    key_lab = bpy.data.objects.new(name="Key Label", object_data=font_curve)
    key_lab.location[0] =  leg_x
    key_lab.location[1] = -leg_y
    key_lab.location[2] = 0.501
    key_lab.scale[0] = max(sizes) * key_scale * 0.5
    key_lab.scale[1] = max(sizes) * key_scale * 0.5
    key_lab.data.materials.append(text_mat)
    key_coll.objects.link(key_lab)
    
# render setup
scene.render.engine = 'CYCLES'
scene.cycles.device = 'GPU'
scene.cycles.preview_samples = 32
scene.cycles.samples = 128
scene.cycles.tile_size = 256
scene.render.resolution_percentage = 400

if statistics == True:
    print('creating statistics...')
    # add key label
    font_curve = bpy.data.curves.new(type="FONT", name="Font Curve")
    font_curve.body = os.path.basename(
    'Directory: ' + os.path.basename(dir_loc) + '\n'
    'Date: ' + date_str + '\n'
    'Time: ' + time_str + '\n'
    'Directory Count: ' + str(len(dirs)) + '\n'
    'Average Size: ' + str(format_bytes(mean(bytes))) + '\n'
    'Largest Directory: ' + str(format_bytes(max(bytes))) + '\n'
    'Deepest Nesting: ' + str(max(depths)) + ' levels'  + '\n'
    )
    stats = bpy.data.objects.new(name="Statistics", object_data=font_curve)
    font_curve.align_x = 'CENTER'
    font_curve.align_y = 'CENTER'
    stats.location[0] = leg_x
    stats.location[2] = 0.001
    stats.scale[0] = max(sizes) * key_scale * 0.25
    stats.scale[1] = max(sizes) * key_scale * 0.25
    stats.data.materials.append(text_mat)
    key_coll.objects.link(stats)

if scene_setup == True:
    # check for collections
    if bpy.data.collections.get('Scene') is not None:    
        sc_coll = bpy.data.collections['Scene']
        setup_scene()
        
    else:        
        sc_coll = bpy.data.collections.new('Scene')
        bpy.context.scene.collection.children.link(sc_coll)
        setup_scene()

print('Done.')
