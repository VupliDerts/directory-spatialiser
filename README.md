# Directory Spatialiser

[Blender 3.5](https://projects.blender.org/blender/blender) Visualised using packed cylinders:

![image](Blender_3.5_Visualisation.jpg)

## About

Directory Spatialiser is a Python script for visualising directory heirarchies in 3D space using packed cylinders.  The script recursively scans directories and uses the data to create a list of circles which are packed and extruded to form a map of the data in Blender's 3D viewport.

- The directory size sets the radius of the circle.
- The depth of the folder sets the height of the cylinder (deeper directories are closer to the ground).
- The name appearing on each cylinder is the name of the directory.

## Requirements

This script uses a python module called [packcircles](https://github.com/mhtchan/packcircles) which needs to be added to Blender's Python installation.  Consider using [this](https://b3d.interplanety.org/en/how-to-install-required-packages-to-the-blender-python-with-pip/) guide which outlines the process of adding a module to Blender.

## Usage

1. Save a Blender file in the top level of the directory you want to analyse.

2. Paste the [python script](https://gitlab.com/VupliDerts/directory-spatialiser/-/blob/ff1978111021dbf7f4d6a9aef3538e1cc55c7ba9/Directory%20Spatialiser%20-%20Packed%20Circles.py) into Blender's text editor.

3. Run the script, following Blender's console window for information on progress.

## Changelog

2023.04.06 - Added statistics, including largest directory, date, time, average directory size etc.

2023.03.21 - Added tagging capability.  When scanning directories the script will search for text files with the name 'readme.txt'. If the words in this file match any of the words in the 'tags' list at the start of the script, then the cylinder objects will be highlighted red. Subfolders are also tagged with the color, but with a less saturation.


